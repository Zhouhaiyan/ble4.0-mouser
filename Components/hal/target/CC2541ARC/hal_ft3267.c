/**************************************************************************************************
 *                                            INCLUDES
 **************************************************************************************************/
#include "hal_mcu.h"
#include "hal_board_cfg.h"
#include "hal_defs.h"
#include "hal_i2c.h"
#include "hal_types.h"
#include "hal_drivers.h"
#include "hal_ft3267.h"
#include "osal.h"
#include "osal_snv.h"
#include "OSAL_Memory.h"

#include <stdarg.h>
#include <stdio.h>
#include "npi.h"

//--------------------- 
//作者：枫之星雨 
//来源：CSDN 
//原文：https://blog.csdn.net/zzfenglin/article/details/51938856 
//版权声明：本文为博主原创文章，转载请附上博文链接！
void MYPrintf(const char* fmt,...)
{
	va_list ap;
	char string[50];
	va_start(ap,fmt);
	vsprintf(string,fmt,ap);
	va_end(ap);
	NPI_WriteTransport(string,strlen(string));
}

/**************************************************************************************************
 *                                        Private Constants
 **************************************************************************************************/

#define INTERVAL_READ_REG               100  /* interval time per read reg unit:ms */
#define TIMEOUT_READ_REG                1000 /* timeout of read reg unit:ms */
#define FTS_I2C_SLAVE_ADDR              0x38
#define MAX_TOUCH_NBR                   1

/**************************************************************************************************
 *                                        Private Macros
 **************************************************************************************************/
//#define FTS_DEBUG(...)                  MYPrintf("DBG>"##__VA_ARGS__)
//#define FTS_ERROR(...)                  MYPrintf("ERR>"##__VA_ARGS__)
//#define FTS_INFO(...)                   MYPrintf("INFO>"##__VA_ARGS__)

#define FTS_DEBUG(...)
#define FTS_ERROR(...)
#define FTS_INFO(...)
#define nResetLow()                     P1_3 = 0
#define nResetHigh()                    P1_3 = 1
#define msleep(x)                       simpleBLE_Delay_1ms(x)
/**************************************************************************************************
 *                                        Functional Declaration
 **************************************************************************************************/

static int fts_i2c_write(unsigned char i2c_address, unsigned char *writebuf, unsigned short writelen);
static int fts_i2c_read( unsigned char i2c_address, unsigned char *writebuf, unsigned short writelen, unsigned  char *readbuf, int readlen);
static int fts_i2c_read_reg(unsigned char i2c_address, u8 regaddr, u8 *regvalue);
static int fts_i2c_write_reg(unsigned char i2c_address, u8 regaddr, u8 regvalue);

int fts_input_init(struct fts_ts_data *ts_data);
int fts_reset_proc(int hdelayms);
int fts_get_ic_information(struct fts_ts_data *ts_data);
int fts_get_chip_types(struct fts_ts_data *ts_data, u8 id_h, u8 id_l, bool fw_valid);
void TP_Test1(void);

void Hal_HW_WaitUs(uint16 microSecs);
void simpleBLE_Delay_1ms(int times);
void MYPrintf(const char* fmt,...);
/**************************************************************************************************
 *                                        Global Variables
 **************************************************************************************************/

struct fts_ts_data *fts_data;


void HalFT3267Init( void )
{
  /* Allocate Memory */
  struct fts_ts_data *ts_data;
  /* Init fts_data */
  ts_data = (struct fts_ts_data *)osal_mem_alloc(sizeof(struct fts_ts_data));
  if (!ts_data) {
      FTS_ERROR("Failed to allocate memory for fts_data\n");
      return;
  }
  fts_data = ts_data;
  int ret = fts_input_init(ts_data);//[ZHY] todo: 初始化HID输入
  if (ret) {
      FTS_ERROR("fts input initialize fail\n");
      goto err_input_init;
  }
  
  /* Set up the I2C interface */
  HalI2CInit( i2cClock_267KHZ ); // I2C clock rate
  
  /* Set I2C Slave Address */
  FTS_I2C_SLAVE_ADDR;
  
  /* Init nReset,Int IO Pin */
  P1DIR |= BV(3);       
  nResetHigh();
  
//  P1DIR &= ~BV(5);       todo: regiest irq
  
  /* Reset FT3267 by asserting Reset Pin for 200ms */
  fts_reset_proc(200);
  
  /* Read IC Info */
  fts_get_ic_information(fts_data);
err_input_init:
  return;
}

int fts_input_init(struct fts_ts_data *ts_data)
{
    int point_num = 0;
    int ret = 0;

    point_num = MAX_TOUCH_NBR;
    ts_data->pnt_buf_size = point_num * FTS_ONE_TCH_LEN + 3;
    ts_data->point_buf = (u8 *)osal_mem_alloc(ts_data->pnt_buf_size);
    if (!ts_data->point_buf) {
        FTS_ERROR("failed to alloc memory for point buf!\n");
        ret = -ENOMEM;
        goto err_point_buf;
    }

    ts_data->events = (struct ts_event *)osal_mem_alloc(point_num * sizeof(struct ts_event));
    if (!ts_data->events) {

        FTS_ERROR("failed to alloc memory for point events!\n");
        ret = -ENOMEM;
        goto err_event_buf;
    }

    return 0;

err_event_buf:
    osal_mem_free(ts_data->events);//(ts_data->point_buf);
    return ret;

err_point_buf:
    osal_mem_free(ts_data->point_buf);
    return ret;
}


/* Reset IC ------------------------------------------------------------------*/
int fts_reset_proc(int hdelayms)
{
  nResetLow();
  simpleBLE_Delay_1ms(20);
  nResetHigh();
  simpleBLE_Delay_1ms(hdelayms);
  return 0;
}

/* Read IC Info --------------------------------------------------------------*/
int fts_get_chip_types(struct fts_ts_data *ts_data, u8 id_h, u8 id_l, bool fw_valid)
{
    int i = 0;
    struct ft_chip_t ctype[] = FTS_CHIP_TYPE_MAPPING;
    u32 ctype_entries = sizeof(ctype) / sizeof(struct ft_chip_t);

    if ((0x0 == id_h) || (0x0 == id_l)) {
        FTS_ERROR("id_h/id_l is 0\n");
        return -EINVAL;
    }

    FTS_DEBUG("verify id:0x%02x%02x\n", id_h, id_l);
    for (i = 0; i < ctype_entries; i++) {
        if (VALID == fw_valid) {
            if ((id_h == ctype[i].chip_idh) && (id_l == ctype[i].chip_idl))
                break;
        } else {
            if (((id_h == ctype[i].rom_idh) && (id_l == ctype[i].rom_idl))
                || ((id_h == ctype[i].pb_idh) && (id_l == ctype[i].pb_idl))
                || ((id_h == ctype[i].bl_idh) && (id_l == ctype[i].bl_idl)))
                break;
        }
    }

    if (i >= ctype_entries) {
        return -ENODATA;
    }

    ts_data->ic_info.ids = ctype[i];
    return 0;
}

int fts_get_ic_information(struct fts_ts_data *ts_data)
{
    static int ret = 0;
    int cnt = 0;
    u8 chip_id[2] = { 0 };
    u8 id_cmd[4] = { 0 };
    u32 id_cmd_len = 0;

    ts_data->ic_info.is_incell = FTS_CHIP_IDC;
    ts_data->ic_info.hid_supported = FTS_HID_SUPPORTTED;
    do {
        ret = fts_i2c_read_reg(FTS_I2C_SLAVE_ADDR, FTS_REG_CHIP_ID, &chip_id[0]);
        ret = fts_i2c_read_reg(FTS_I2C_SLAVE_ADDR, FTS_REG_CHIP_ID2, &chip_id[1]);
        if ((ret <= 0) || (0x0 == chip_id[0]) || (0x0 == chip_id[1])) {
            FTS_DEBUG("i2c read invalid, read:0x%02x%02x\n", chip_id[0], chip_id[1]);
        } else {
            ret = fts_get_chip_types(ts_data, chip_id[0], chip_id[1], VALID);
            if (!ret)
                break;
            else
                FTS_DEBUG("TP not ready, read:0x%02x%02x\n", chip_id[0], chip_id[1]);
        }

        cnt++;
        msleep(INTERVAL_READ_REG);
    } while ((cnt * INTERVAL_READ_REG) < TIMEOUT_READ_REG);

    if ((cnt * INTERVAL_READ_REG) >= TIMEOUT_READ_REG) {
        FTS_INFO("fw is invalid, need read boot id\n");
//        if (ts_data->ic_info.hid_supported) {
//            fts_i2c_hid2std(FTS_I2C_SLAVE_ADDR);
//        }

        id_cmd[0] = FTS_CMD_START1;
        id_cmd[1] = FTS_CMD_START2;
        ret = fts_i2c_write(FTS_I2C_SLAVE_ADDR, (unsigned char *)id_cmd, 2);
        if (ret <= 0) {
            FTS_ERROR("start cmd write fail\n");
            return ret;
        }

        msleep(FTS_CMD_START_DELAY);
        id_cmd[0] = FTS_CMD_READ_ID;
        id_cmd[1] = id_cmd[2] = id_cmd[3] = 0x00;
        if (ts_data->ic_info.is_incell)
            id_cmd_len = FTS_CMD_READ_ID_LEN_INCELL;
        else
            id_cmd_len = FTS_CMD_READ_ID_LEN;
        ret = fts_i2c_read(FTS_I2C_SLAVE_ADDR, (unsigned char *)id_cmd, id_cmd_len, (unsigned char *)chip_id, 2);
        if ((ret <= 0) || (0x0 == chip_id[0]) || (0x0 == chip_id[1])) {
            FTS_ERROR("read boot id fail\n");
            return -EIO;
        }
        ret = fts_get_chip_types(ts_data, chip_id[0], chip_id[1], INVALID);
        if (ret <= 0) {
            FTS_ERROR("can't get ic informaton\n");
            return ret;
        }
    }

    FTS_INFO("get ic information, chip id = 0x%02x%02x\n",
             ts_data->ic_info.ids.chip_idh, ts_data->ic_info.ids.chip_idl);

    return 0;
}
#if 0
/*****************************************************************************
*  Name: fts_read_touchdata
*  Brief:
*  Input:
*  Output:
*  Return: return 0 if succuss
*****************************************************************************/
int fts_read_touchdata(struct fts_ts_data *data)
{
    int ret = 0;
    int i = 0;
    u8 pointid;
    int base;
    struct ts_event *events = data->events;
    int max_touch_num = MAX_TOUCH_NBR;
    u8 *buf = data->point_buf;

    data->point_num = 0;
    data->touch_point = 0;

    memset(buf, 0xFF, data->pnt_buf_size);
    buf[0] = 0x00;

    ret = fts_i2c_read(FTS_I2C_SLAVE_ADDR, (unsigned char *)buf, 1, (unsigned char *)buf, data->pnt_buf_size);
    if (ret < 0) {
        FTS_ERROR("read touchdata failed, ret:%d", ret);
        return ret;
    }
    data->point_num = buf[FTS_TOUCH_POINT_NUM] & 0x0F;

    if (data->ic_info.is_incell) {
        if ((data->point_num == 0x0F) && (buf[1] == 0xFF) && (buf[2] == 0xFF)
            && (buf[3] == 0xFF) && (buf[4] == 0xFF) && (buf[5] == 0xFF) && (buf[6] == 0xFF)) {
            FTS_INFO("touch buff is 0xff, need recovery state");
            fts_tp_state_recovery(FTS_I2C_SLAVE_ADDR);
            return -EIO;
        }
    }
    if (data->point_num > max_touch_num) {
        FTS_INFO("invalid point_num(%d)", data->point_num);
        return -EIO;
    }

#if (FTS_DEBUG_EN)
    fts_show_touch_buffer(buf, data->point_num);
#endif

    for (i = 0; i < max_touch_num; i++) {
        base = FTS_ONE_TCH_LEN * i;

        pointid = (buf[FTS_TOUCH_ID_POS + base]) >> 4;
        if (pointid >= FTS_MAX_ID)
            break;
        else if (pointid >= max_touch_num) {
            FTS_ERROR("ID(%d) beyond max_touch_number", pointid);
            return -EINVAL;
        }

        data->touch_point++;

        events[i].x = ((buf[FTS_TOUCH_X_H_POS + base] & 0x0F) << 8) +
                      (buf[FTS_TOUCH_X_L_POS + base] & 0xFF);
        events[i].y = ((buf[FTS_TOUCH_Y_H_POS + base] & 0x0F) << 8) +
                      (buf[FTS_TOUCH_Y_L_POS + base] & 0xFF);
        events[i].flag = buf[FTS_TOUCH_EVENT_POS + base] >> 6;
        events[i].id = buf[FTS_TOUCH_ID_POS + base] >> 4;
        events[i].area = buf[FTS_TOUCH_AREA_POS + base] >> 4;
        events[i].p =  buf[FTS_TOUCH_PRE_POS + base];

        if (EVENT_DOWN(events[i].flag) && (data->point_num == 0)) {
            FTS_INFO("abnormal touch data from fw");
            return -EIO;
        }
    }
    if (data->touch_point == 0) {
        FTS_INFO("no touch point information");
        return -EIO;
    }

    return 0;
}

int fts_wait_tp_to_valid(void)
{
    int ret = 0;
    int cnt = 0;
    u8 reg_value = 0;
    u8 chip_id = fts_data->ic_info.ids.chip_idh;

    do {
        ret = fts_i2c_read_reg(FTS_I2C_SLAVE_ADDR, FTS_REG_CHIP_ID, &reg_value);
        if ((ret < 0) || (reg_value != chip_id)) {
            FTS_DEBUG("TP Not Ready, ReadData = 0x%x", reg_value);
        } else if (reg_value == chip_id) {
            FTS_INFO("TP Ready, Device ID = 0x%x", reg_value);
            return 0;
        }
        cnt++;
        msleep(INTERVAL_READ_REG);
    } while ((cnt * INTERVAL_READ_REG) < TIMEOUT_READ_REG);

    return -EIO;
}


void fts_show_touch_buffer(u8 *buf, int point_num)
{
    int len = point_num * FTS_ONE_TCH_LEN;
    int count = 0;
    int i;
    static char g_sz_debug[1024] = {0};

    memset(g_sz_debug, 0, 1024);
    if (len > (fts_data->pnt_buf_size - 3)) {
        len = fts_data->pnt_buf_size - 3;
    }
    else if (len == 0) {
        len += FTS_ONE_TCH_LEN;
    }
    count += sprintf(g_sz_debug, "%02X,%02X,%02X", buf[0], buf[1], buf[2]);
    for (i = 0; i < len; i++) {
        count += sprintf(g_sz_debug + count, ",%02X", buf[i + 3]);
    }
    FTS_DEBUG("buffer: %s", g_sz_debug);
}



#endif

/* Read Touch Points ---------------------------------------------------------*/
typedef struct {
  uint8 x;
  uint8 y;
}TP_POINT;


#define CTP_I2C_READ(u8Reg,pu8Value,u8Len)      do{uint8 reg = u8Reg;fts_i2c_read(FTS_I2C_SLAVE_ADDR, (unsigned char *)&reg, 1, (unsigned char *)pu8Value, u8Len);}while(0)


enum {
	TP_Stu_NoPress = 0,//初始值
	TP_Stu_Press = 1,//按下状态
	TP_Stu_DoPress = 2,//可以处理	
};

#define X_LENGTH                854
#define Y_LENGTH                480
#define CTP_ACK_COUNTER         10
#define TD_STAT_ADDR            0x2
#define TT_MODE_BUFFER_INVALID  0x08
#define TD_STAT_NUMBER_TOUCH    0x07
#define TOUCH1_XH_ADDR          0x03
#define TOUCH2_XH_ADDR          0x09
#define TOUCH3_XH_ADDR          0x0F
#define TOUCH4_XH_ADDR          0x15



static uint8 TpStuV = 0;
uint8 Tp_PressStaus = 0;//tp读取按下状态
static uint16 TpComOutCount;//按下状态下的通讯数目
static uint16 TpComOutFlag;
static uint8 values[4];
static TP_POINT tpPoint;

static void CTP_GetTpOnePoint(const uint8 x_base,TP_POINT *point,uint8 *tpStu)
{
	
	CTP_I2C_READ(x_base, values, 4);
        FTS_DEBUG("Reg 0x%02X:%02X %02X %02X %02X\n",x_base,values[0],values[1],values[2],values[3]);
	if(x_base == 0x3)
	*tpStu = (values[0]>>6)&0x3;//(values[0]&0xC0);//
	point->x = (((uint16)(values[0]&0x0f))<<8) | values[1];
	point->y =   (((uint16)(values[2]&0x0f))<<8) | values[3];
}

static uint8 CTP_GetTpPoint(TP_POINT *Tmp_point,uint8 *TpStu)
{
	uint8 counter = 0,i;
	uint8 lvalue;
	uint8 PointsNum;
	TP_POINT tp;
	int x=0;
	int y=0;
	const uint8 x_base[] = {TOUCH1_XH_ADDR, TOUCH2_XH_ADDR, TOUCH3_XH_ADDR, TOUCH4_XH_ADDR};
//	do{ //make sure data in buffer is valid
//		CTP_I2C_READ(TD_STAT_ADDR, &lvalue, 1);
//	
//		if(counter++ == 0x30){
//		return 0;
//		}
//                Hal_HW_WaitUs(100);
//	}while(lvalue & TT_MODE_BUFFER_INVALID); 	

	CTP_I2C_READ(TD_STAT_ADDR, &lvalue, 1); 
	PointsNum = (uint8)(lvalue & TD_STAT_NUMBER_TOUCH);
        if(PointsNum == 0)
          return 0;
        FTS_DEBUG("Reg 0x%02X:%02X\n",TD_STAT_ADDR,lvalue);
	if((PointsNum == 0) || (PointsNum > 5))
		PointsNum = 1;
	for(i=0;i<PointsNum;i++)//
	{
		CTP_GetTpOnePoint(x_base[i], &tp,TpStu);
		x += tp.x;
		y += tp.y;
	}
	Tmp_point->x = x/PointsNum;
	Tmp_point->y = y/PointsNum;
	return 1;
}

//读取触摸
uint8  ReadTpPoint(void)
{
	int x=0;
	int y=0;

	TP_POINT tptmpPoint;	
	TpStuV = 0;
	if(CTP_GetTpPoint(&tptmpPoint,&TpStuV))
	{
#if 0
			if(TpStuV == 1)
			{
				//此处代表手指松开
				if(Tp_PressStaus ==TP_Stu_Press)
				{
				Tp_PressStaus = TP_Stu_DoPress;
				}
				TpComOutCount = 0;
				return 1;
			}
#else
                        
			x=tptmpPoint.x;//得到X
			y=tptmpPoint.y;//得到y	
				//换算成触摸点
				tpPoint.x=y;
				tpPoint.y=x;
                                return 1;
#endif
			x=tptmpPoint.x;//得到X
			y=tptmpPoint.y;//得到y				
			
			if(Tp_PressStaus == TP_Stu_NoPress)
			{
				//换算成触摸点
				tpPoint.x=y;
				tpPoint.y=x;
			}
			
			Tp_PressStaus = TP_Stu_Press;

			return 1;
	}
	else
	{
			//校验失败
			x=-1;
			y=-1;
			return 0;
	}
}

void TP_Test1(void){
  if(ReadTpPoint()){
    FTS_INFO("Valide Touch: [%d,%d]\n\n",tpPoint.x,tpPoint.y);
  }
//  else{
//    FTS_INFO("Touch Not Ready\n");
//  }
    
}


int FT3267_ReadMotion(int16* pusX, int16* pusY){
  if(ReadTpPoint()){
    FTS_INFO("Valide Touch: [%d,%d]\n\n",tpPoint.x,tpPoint.y);
    *pusX = tpPoint.x;
    *pusY = tpPoint.y;
    return 1;
  }
  return 0;
  
}
/* IIC Abstraction -----------------------------------------------------------*/
static int fts_i2c_write_reg(unsigned char i2c_address, u8 regaddr, u8 regvalue)
{
    u8 buf[2] = {0};
    buf[0] = regaddr;
    buf[1] = regvalue;
    return fts_i2c_write(i2c_address, buf, sizeof(buf));
}

static int fts_i2c_read_reg(unsigned char i2c_address, u8 regaddr, u8 *regvalue)
{
    return fts_i2c_read(i2c_address, &regaddr, 1, regvalue, 1);
}

static int fts_i2c_read(unsigned char i2c_address, unsigned char *writebuf, unsigned short writelen,unsigned  char *readbuf, int readlen)
{
  int ret;
  HalI2CWrite( i2c_address, writelen, writebuf );
  Hal_HW_WaitUs(50);
  ret = HalI2CRead( i2c_address, readlen, readbuf);
  return ret;
}

static int fts_i2c_write(unsigned char i2c_address, unsigned char *writebuf, unsigned short writelen)
{
    /* Send address and data */
    return HalI2CWrite( i2c_address, writelen, writebuf );
//    return I2CM_WriteNBytesFrom(writebuf+1, i2c_address, writebuf[0],writelen-1);
}



/* OS Abstraction ------------------------------------------------------------*/
uint16 HalFT3267HandleOSEvent( uint16 events )
{
  uint16 rtnVal = 0;

//  if (events & HAL_MOTION_MEASUREMENT_START_EVENT)
//  {
//    HalMotionHandleMeasurementStartEvent();
//    rtnVal = HAL_MOTION_MEASUREMENT_START_EVENT;
//  }
  return rtnVal;
}


/* Miscellaneous -------------------------------------------------------------*/
void Hal_HW_WaitUs(uint16 microSecs)
{
  while(microSecs--)
  {
    /* 32 NOPs == 1 usecs */
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop"); asm("nop"); asm("nop"); asm("nop");
    asm("nop"); asm("nop");
  }
}




/**************************************************************************************************
**************************************************************************************************/
