#ifndef __I2CM_H__
#define __I2CM_H__	 

#include <stdint.h>

void Hal_HW_WaitUs(uint16 microSecs);
#define I2CM_BACK2BACK_WR_DELAY()	Hal_HW_WaitUs(20)
#define I2CM_USDELAY(X) 		Hal_HW_WaitUs(X)


  P1DIR |= BV(3);       
#define I2CM_SCK_OUTPUT()		
#define I2CM_SCK_LOW()			PBout(6)=0
#define I2CM_SCK_HIGH()			PBout(6)=1



#define I2CM_SDA_INPUT()		{GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=0x80000000;}
#define I2CM_READ_SDA()			PBin(7)
#define I2CM_SDA_OUTPUT()		{GPIOB->CRL&=0X0FFFFFFF;GPIOB->CRL|=0x30000000;}
#define I2CM_SDA_HIGH()			PBout(7) = 1
#define I2CM_SDA_LOW()			PBout(7) = 0

#define CLOCK_RATE_US   4
void I2CM_Init(void);
int I2CM_WriteNBytesFrom(uint8_t *src, uint8_t SlaveAddress, uint8_t RegAddress,uint8_t NbrOfBytes);
int I2CM_ReadNBytesTo(uint8_t *dest, uint8_t SlaveAddress, uint8_t RegAddress,uint8_t NbrOfBytes);



#endif
