#include "i2cm.h"



static void i2cm_start ( void )
{	
	I2CM_SDA_OUTPUT();
	I2CM_SDA_HIGH();  
	I2CM_USDELAY(CLOCK_RATE_US);
	I2CM_SCK_HIGH();   
	I2CM_USDELAY(CLOCK_RATE_US);     
	I2CM_SDA_LOW();   
	I2CM_USDELAY(CLOCK_RATE_US); 		
	I2CM_SCK_LOW(); 
	I2CM_USDELAY(CLOCK_RATE_US);
}


static void i2cm_stop ( void )
{	
	I2CM_SDA_OUTPUT();
	I2CM_SDA_LOW(); 
	I2CM_USDELAY(CLOCK_RATE_US); 
	I2CM_SCK_HIGH(); 
	I2CM_USDELAY(CLOCK_RATE_US);
	I2CM_SDA_HIGH(); 
	I2CM_USDELAY(CLOCK_RATE_US);
}


static unsigned char i2cm_send_byte ( unsigned char send_data )
{
	unsigned char bit_cnt;
	unsigned char	b_ack=0;
	unsigned char i=200;
	
	I2CM_SDA_OUTPUT();
	for( bit_cnt=0; bit_cnt<8; bit_cnt++ ) 
	{ 
		I2CM_SCK_LOW(); 
		if((send_data<<bit_cnt) & 0x80) 
			I2CM_SDA_HIGH();  
		else 
			I2CM_SDA_LOW();  
		I2CM_USDELAY(CLOCK_RATE_US); 
		
		I2CM_SCK_HIGH();       
		I2CM_USDELAY(CLOCK_RATE_US);          
	}
	
	I2CM_SCK_LOW();
	I2CM_SDA_INPUT();
	I2CM_USDELAY(CLOCK_RATE_US);
	
	I2CM_SCK_HIGH();
	I2CM_USDELAY(CLOCK_RATE_US);
	
	i = 200;
	while(i--)
	{
		I2CM_USDELAY(CLOCK_RATE_US);
		if(I2CM_READ_SDA()==0)
		{
			b_ack = 1;
			break;
		}  
	}
	
	I2CM_SCK_LOW();
	I2CM_USDELAY(CLOCK_RATE_US);
	return b_ack;
}


static unsigned char i2cm_read_byte ( void )
{
	unsigned char read_value=0;
	unsigned char bit_cnt;
	I2CM_SDA_INPUT();
	for ( bit_cnt=0; bit_cnt<8; bit_cnt++ )
	{
		I2CM_SCK_HIGH();       
		I2CM_USDELAY(CLOCK_RATE_US);
		read_value <<= 1;
		if ( I2CM_READ_SDA()==1 ) 
			read_value +=1;
		I2CM_SCK_LOW();     
		I2CM_USDELAY(CLOCK_RATE_US);
	}
	return (read_value);
}


static void i2cm_ack ( void )
{	
	I2CM_SDA_OUTPUT();
	I2CM_SDA_LOW();   
	I2CM_USDELAY(CLOCK_RATE_US);
	I2CM_SCK_LOW();
	I2CM_USDELAY(CLOCK_RATE_US);      
	I2CM_SCK_HIGH();
	I2CM_USDELAY(CLOCK_RATE_US);
	I2CM_SCK_LOW();    
	I2CM_USDELAY(CLOCK_RATE_US); 
	I2CM_SDA_HIGH();
	I2CM_USDELAY(CLOCK_RATE_US);
}


static void i2cm_nack ( void )
{	
	I2CM_SDA_OUTPUT();
	I2CM_SDA_HIGH(); 
	I2CM_USDELAY(CLOCK_RATE_US);      
	I2CM_SCK_HIGH();
	I2CM_USDELAY(CLOCK_RATE_US);
	I2CM_SCK_LOW(); 
}


void I2CM_Init(void){
	I2CM_SCK_OUTPUT();	
	I2CM_SDA_INPUT();
}


int I2CM_WriteNBytesFrom(uint8_t *src, uint8_t SlaveAddress, uint8_t RegAddress,uint8_t NbrOfBytes){
	int i;
	
	i2cm_start();               
	if(0 == i2cm_send_byte ( SlaveAddress+0 )){//写命令
		goto __fail;
	}

	if(0 == i2cm_send_byte( RegAddress )){//写寄存器地址
		goto __fail;
	}
	
	for(i=0;i<NbrOfBytes;i++){
		if(0 == i2cm_send_byte ( src[i] )){//写N个字节到从机
			goto __fail;
		}
	}
	
	//发送停止位
	i2cm_stop();
	return NbrOfBytes;
	__fail:
	i2cm_stop();
	return -1;

}


int I2CM_ReadNBytesTo(uint8_t *dest, uint8_t SlaveAddress, uint8_t RegAddress,uint8_t NbrOfBytes){
	int i;
	uint8_t temp;
	/* 1. 告诉从机起始寄存器地址 ------------------------------- */
	i2cm_start();               
	if(0 == i2cm_send_byte ( SlaveAddress+0 )){//写命令
		goto __fail;
	}

	if(0 == i2cm_send_byte( RegAddress )){//写寄存器地址
		goto __fail;
	}
	i2cm_stop();
	
	I2CM_BACK2BACK_WR_DELAY();
	
	/* 2. 向从机读N个字节 -------------------------------------- */
	i2cm_start();			 	//重新发起始信号
	if(0==i2cm_send_byte(SlaveAddress+1)){//读命令
		goto __fail;
	}
	for(i=0;i<NbrOfBytes;i++){
		temp = i2cm_read_byte ();//读取温湿度的高位字节
		if(i+1 < NbrOfBytes){
			i2cm_ack();//mcu应答
		}
		else{
			i2cm_nack();//mcu无应答
		}
		if(dest != 0){
			dest[i] = temp;
		}
	}
	
	i2cm_stop();
	return NbrOfBytes;
	__fail:
	i2cm_stop();
	return -1;
}



